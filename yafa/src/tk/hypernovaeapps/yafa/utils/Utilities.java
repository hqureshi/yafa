package tk.hypernovaeapps.yafa.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v4.app.ShareCompat;
import android.widget.Toast;

public class Utilities {

	public static String PKG_GOOGLE_PLUS = "com.google.android.apps.plus";
	
	public static void ToastIt(Context context, String sText) {
		if (sText == null || sText.length() == 0)
			return;
		Toast.makeText(context, sText, Toast.LENGTH_SHORT).show();
	}

	public static boolean isGooglePlusInstalled(Context context) {
		try {
			context.getPackageManager().getApplicationInfo(PKG_GOOGLE_PLUS, 0);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}

	public static void ShareOnGooglePlus(Activity launchingActivity, Context context, String sAppURL) {
		if (isGooglePlusInstalled(context)) {
			Intent shareIntent = ShareCompat.IntentBuilder.from(launchingActivity)
					.setText("Found a cool App! - " + sAppURL)
					.setType("text/plain")
					.getIntent()
					.setPackage(PKG_GOOGLE_PLUS);
			context.startActivity(shareIntent);
		} else {
			String sShareURL = "https://plus.google.com/share?url=" + sAppURL;
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(sShareURL)));
		}
	}

	// public static int convertDPItoPixels(int nDPI, Resources res)
	// {
	// return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, nDPI, res.getDisplayMetrics());
	// }
	//
	// public static int convertPixelsToDPI(int nPixels, Resources res)
	// {
	// return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, nPixels, res.getDisplayMetrics());
	// }

	public static int dpFromPx(int px, Resources res) {
		return (int) (px / res.getDisplayMetrics().density);
	}

	public static int pxFromDp(int dp, Resources res) {
		return (int) (dp * res.getDisplayMetrics().density);
	}
}
