package tk.hypernovaeapps.yafa;

import java.util.Locale;

import tk.hypernovaeapps.yafa.utils.Utilities;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Build;
import android.view.SurfaceHolder;

/**
 * @author Habib Ahmed Qureshi
 * 
 */
// http://stackoverflow.com/questions/6068803/how-turn-on-camera-flash-light-programmatically-in-android
public class CFlashLight {

	private Camera camera;
	private Context _Context;

	public boolean IsAvailable(Context context) {
		_Context = context.getApplicationContext();
		return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
	}

	// TODO: LED flashlight on Galaxy Nexus
	// http://stackoverflow.com/questions/8876843/led-flashlight-on-galaxy-nexus-controllable-by-what-api
	// http://stackoverflow.com/questions/10734858/opening-flashlight-of-galaxy-nexus
	public boolean TurnOn() {
		try {
			String sDevice_Model = Build.MODEL;
			// To handle "Samsung Galaxy Tab"
			boolean bSamsungGalaxyTab = sDevice_Model.equals("GT-P1000") || sDevice_Model.equals("GT-P3100")
					|| sDevice_Model.equals("GT-P5100") || sDevice_Model.equals("GT-P3110")
					|| sDevice_Model.equals("GT-P7500");
			// To handle "Samsung Galaxy Ace"
			boolean bSamsungGalaxyAce = sDevice_Model.equals("GT-5830L") || sDevice_Model.equals("GT-S5830")
					|| sDevice_Model.equals("GT-S5839i") || sDevice_Model.equals("GT-S7560M");

			if (android.os.Build.MANUFACTURER.toLowerCase(Locale.ENGLISH).contains("motorola")) { // To handle
																																// "Motorola Droid"
				DoMotorolaOn();
				return true;
			}
			if (camera == null)
				camera = Camera.open();
			Parameters params = camera.getParameters();
			if (bSamsungGalaxyTab || bSamsungGalaxyAce)
				params.setFlashMode(Parameters.FLASH_MODE_ON);
			else
				params.setFlashMode(Parameters.FLASH_MODE_TORCH);
			camera.setParameters(params);

			if (bSamsungGalaxyAce) // http://stackoverflow.com/questions/6939816/turn-on-off-camera-led-flash-light-in-samsung-galaxy-ace-2-2-1-galaxy-tab
			{
				params = camera.getParameters();
				params.setFlashMode(Parameters.FLASH_MODE_OFF);
				camera.setParameters(params);
			}

			camera.startPreview();
			if (bSamsungGalaxyTab || bSamsungGalaxyAce) // This might be required to do only once - check if we get the bug
																		// on the device
				camera.autoFocus(new Camera.AutoFocusCallback() {
					public void onAutoFocus(boolean success, Camera camera) {
					}
				});

			return true;
		} catch (Exception ex) {
			try {
				// Ignoring this because I am getting this exception when I am resuming the activity and implemented
				// SurfaceHolder.Callback interface. And after ignoring still works perfect @ Nexus 4
				if (ex.getMessage().contains("startPreview failed"))
					return true;
				Utilities.ToastIt(_Context, ex.getMessage());
				camera.release();
			} finally {
			}
			return false;
		}
	}

	public boolean TurnOff() {
		try {
			if (android.os.Build.MANUFACTURER.toLowerCase(Locale.ENGLISH).contains("motorola")) {
				DoMotorolaOff();
				return true;
			}
			if (camera == null)
				return false;
			Parameters p = camera.getParameters();
			p.setFlashMode(Parameters.FLASH_MODE_OFF);
			camera.setParameters(p);
			camera.stopPreview();
			// camera.release(); //Gives me error on nexus 4
			return true;
		} catch (Exception ex) {
			Utilities.ToastIt(_Context, ex.getMessage());
			try {
				camera.release();
			} finally {
			}
			return false;
		}
	}

	// Because of SurfaceHolder - which is because of Samsung and other sort of devices w.r.t Camera/Flash
	public void setPreviewDisplay(SurfaceHolder holder) {
		if (camera == null)
			return;
		try {
			camera.setPreviewDisplay(holder);
		} catch (Exception ex) {
			Utilities.ToastIt(_Context, ex.getMessage());
		}
	}

	public boolean CompleteOff() {
		try {
			if (android.os.Build.MANUFACTURER.toLowerCase(Locale.ENGLISH).contains("motorola"))
				return true; // Do nothing in this case
			if (camera == null)
				return false;
			camera.stopPreview();
			camera.release();
			camera = null;
			return true;
		} catch (Exception ex) {
			Utilities.ToastIt(_Context, ex.getMessage());
			return false;
		}
	}

	private void DoMotorolaOn() {
		DroidLED led;
		try {
			led = new DroidLED();
			led.enable(true);
		} catch (Exception ex) {
			Utilities.ToastIt(_Context, ex.getMessage());
		}

	}

	private void DoMotorolaOff() {
		DroidLED led;
		try {
			led = new DroidLED();
			led.enable(false);
		} catch (Exception ex) {
			Utilities.ToastIt(_Context, ex.getMessage());
		}
	}
}
