package tk.hypernovaeapps.yafa;

import tk.hypernovaeapps.yafa.R;
import tk.hypernovaeapps.yafa.utils.Utilities;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ToggleButton;

/**
 * Yet Another Flashlight App - YAFA now renamed to FlashMe but keeping the same namespace
 * 
 * @author Habib Ahmed Qureshi
 * 
 *         SurfaceHolder - mainly added to handle Samsung
 *         http://stackoverflow.com/questions/8876843/led-flashlight-on-galaxy
 *         -nexus-controllable-by-what-api/9379765#9379765
 */
public class MainActivity extends Activity implements SurfaceHolder.Callback {

	CFlashLight flash = new CFlashLight();
	public static SurfaceView surfaceView;
	public static SurfaceHolder surfaceHolder;
	private final String sCRLF = System.getProperty("line.separator");
	boolean bDarkBackground;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		bDarkBackground = true;

		this.setTitle(getResources().getString(R.string.app_name_full));
		
		//ToggleButton Image setup
		Button btn = (Button) this.findViewById(R.id.toggle);
		ViewGroup.LayoutParams RelLOParams = btn.getLayoutParams();
		RelLOParams.width = Utilities.pxFromDp(144,getResources());
		RelLOParams.height = Utilities.pxFromDp(144,getResources());
		btn.setLayoutParams(RelLOParams);

		surfaceView = (SurfaceView) this.findViewById(R.id.surfaceview);
		surfaceHolder = surfaceView.getHolder();
		surfaceHolder.addCallback(this);
		surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS); // I am supporting older than API11 as well.

		if (flash.IsAvailable(getApplicationContext())) // Turn on the flash by default
			flash.TurnOn();

		DisablePhoneSleep();
	}

	@Override
	public void onResume() {
		super.onResume();
		HandleFlashToggle();
	}

	// If focus is shift to some other App (activity) then stop the flash (for ex: minimized)
	@Override
	public void onPause() {
		super.onPause();
		flash.TurnOff();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		flash.CompleteOff();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.menuitem_rate:
			RateMyApp();
			return true;
		case R.id.menuitem_like:
			LikeUs();
			return true;
		case R.id.menuitem_share:
			ShareMyApp();
			return true;
		case R.id.menuitem_feedback:
			GiveFeedback();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void AlternateBackgroundColor(View view) {
		if (bDarkBackground) {
			view.setBackgroundColor(Color.WHITE);
			bDarkBackground = false;
		} else {
			view.setBackgroundColor(0xFF222222);
			bDarkBackground = true;
		}
	}

	// Clicking the activity itself changes its colour.
	public void OnActivityClick(View view) {
		AlternateBackgroundColor(view);
	}

	private void HandleFlashToggle() {
		boolean bToggle = ((ToggleButton) this.findViewById(R.id.toggle)).isChecked();
		if (flash.IsAvailable(this.getApplicationContext())) {
			if (bToggle)
				bToggle = flash.TurnOn();
			else
				flash.TurnOff();
		} else
			Utilities.ToastIt(this.getApplicationContext(),
					getResources().getString(R.string.errmsg_flashlight_notavailable));
	}

	public void OnPressToggle(View view) {
		HandleFlashToggle();
	}

	// It disable Sleep function of the phone but restores it automatically after the App is closed.
	private void DisablePhoneSleep() {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	// SurfaceHolder interface implementation
	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// Do nothing
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		flash.setPreviewDisplay(holder);
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// Do nothing
	}

	// Menu Functions
	private void RateMyApp() {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("market://details?id=tk.hypernovaeapps.yafa"));
		startActivity(intent);
	}
	
	private void ShareMyApp() {
		String sAppURL = "https://play.google.com/store/apps/details?id=tk.hypernovaeapps.yafa";
		Utilities.ShareOnGooglePlus(MainActivity.this,this, sAppURL);
//		Intent sharingIntent = new Intent(Intent.ACTION_SEND);
//		sharingIntent.setClassName("com.twitter.android", "com.twitter.android.PostActivity");
//		sharingIntent.putExtra(Intent.EXTRA_TEXT, "dhummy share");
//		startActivity(sharingIntent);
		
		//Share global - to chose any app to share
//		Intent intent = new Intent(Intent.ACTION_SEND);
//		intent.setType("text/plain");
//		intent.putExtra(Intent.EXTRA_TEXT, sAppURL);
//		startActivity(Intent.createChooser(intent, "Share with"));
	}
	
	private void LikeUs() {
		String sFBURL = "https://www.facebook.com/HypernovaeApps";
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(sFBURL)));
	}

	private void GiveFeedback() {
		Intent send = new Intent(Intent.ACTION_SENDTO);
		String uriText = "mailto:"
				+ Uri.encode(getResources().getString(R.string.feedback_emailaddrs))
				+ "?subject="
				+ Uri.encode("[FlashMe] Feedback")
				+ "&body="
				+ Uri.encode("[FlashMe] Feedback (General/Bug/Feature request):" + sCRLF + "Device: " + android.os.Build.MODEL
						+ sCRLF);
		Uri uri = Uri.parse(uriText);

		send.setData(uri);
		startActivity(Intent.createChooser(send, "Send FlashMe feedback email.."));
		/*
		 * Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts( "mailto","abc@gmail.com", null));
		 * emailIntent.putExtra(Intent.EXTRA_SUBJECT, "EXTRA_SUBJECT"); startActivity(Intent.createChooser(emailIntent,
		 * "Send email..."));
		 */
	}
}
